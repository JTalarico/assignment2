({  
     openModal: function(component) {
         component.set('v.showModal', true);
     },

     cancel: function(component) {
         component.set('v.showModal', false);
		 component.set('v.message', 'Cancel');
     },

     save: function(component, event, helper) {
         component.set('v.showModal', false);
         component.set('v.message', 'Confirm');
     }
})